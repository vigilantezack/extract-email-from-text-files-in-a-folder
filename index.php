<?php
    // Grab config vars
    include_once("config.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Email Extractor</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>

<body>
<div id="container">

<h1>Bounce Email Extractor</h1>
<p class="subtitle">Download the source email files from your email server. Place them in the "sources" directory
 in this script. Click the button below to extract the email addresses.</p>
 
<p><a id="showmore" href="" onClick="document.getElementById('moreinfo').style.display = 'block';this.style.display = 'none';return false;">Click here to read more...</a><br />
    <div id="moreinfo">

        <h2>Background</h2>
        <p>We have a web forum with vBulletin that is used often to send emails to the entire forum. Often email addresses go bad
        or they were spam registrations. There is no automated way to remove these users or remove their email from mailings. There is 
        no automated way to even get a list of bounced email addresses. This is not too hard to deal with if I get a few bounces here
        and there, but I needed to process over 3000 bounces that would return when a mailing went out!</p>

        <p>Currently, I have my mail server setup so that all bounced email is delivered into a folder in my primary inbox. 
        This lets me browse the emails, but there is no method in my email client (or any client I know of) to extract the failed
        email address from the server bounce messages en masse.</p>

        <p>This tool was built to scan the raw text of the raw email files to extract the bounced email from the "X-Failed-Recipients"
        email header using a regular expression.</p>

        <p>Once a list of emails is created, I can automate removing these users, changing their usergroup, or erasing their bad email
        from the forum, thus preventing more email from bouncing.</p>

        <p>This tool could be used, with slight modifications, to essentially scan a folder of files, and run a regex on the contents
        of every file in the folder, extracting whatever data you need into a text file.</p>
       
        <br /><a href="" onClick="document.getElementById('moreinfo').style.display = 'none';document.getElementById('showmore').style.display = 'block';return false;">Hide</a>
    </div>
</p>


<?php

if(isset($_POST['process']) && $_POST['process'] == "true") {

    // This lets me flush output as script runs, so they can be updated on progress  
    @apache_setenv('no-gzip', 1);
    @ini_set('zlib.output_compression', 0);
    @ini_set('implicit_flush', 1);
    for ($i = 0; $i < ob_get_level(); $i++) { ob_end_flush(); }
    ob_implicit_flush(1);

    // Set regex to find emails
    $pattern = '/(?:X-Failed-Recipients:\s{1})(.+@.+)/';

    // Go through directory
    $dir = new DirectoryIterator($config['path']);
    $output = fopen($config['output'], "w");
    
    // Echo our progress
    echo "<br /><p>Processing (1 dot = 1 file) </p>";
    flush();

    // Counter so dots go to 80 columns, and we can count files processed
    $cnt = 1;

    foreach ($dir as $fileinfo) {
        
        if (!$fileinfo->isDot()) {

            // Read files one by one
            $file = fopen($config['path'] . "/" . $fileinfo->getFilename(), "r");
            $contents = fread($file, 1100);
            
            // Check for pattern match, add the email to output
            if (preg_match($pattern, $contents, $email)) {
                fwrite($output, $email[1] . "\r\n");
                echo ".";
                // Only output 80 columns in progress report
                if($cnt % 80 == 0) { echo "<br />"; }
                $cnt++;
                flush();
            }

        }
    }

    // Final report
    $cnt--;
    echo "<br /><p>Finished!</p><p>$cnt Files Processed</p>";
    echo "<p><a href='$config[output]'>Open the output file</a></p>";

} else {
    
    // Show the Process button
?>
    <form method="post">
        <input type="hidden" name="process" value="true" />
        <input type="submit" name="Submit" value="Process Folder" />
    </form>

<?php

// If output file already exists
    if(file_exists($config['output'])) {
    
        echo "<p>Output file exists, <a href='$config[output]'>open here.</a></p>";

    }

}

?>

</div>
</body>
</html>